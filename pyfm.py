# -*- coding: utf-8 -*-

import datetime
import getopt
import io
import json
import os
import requests
import sys
import time
import urllib.request
from collections import Counter
from contextlib import redirect_stdout
from utils import *

# Syntax:
# python pyfm.py (-key value)*
# 
# :: Possible keys:
# Shorttag          |   Longtag             Explanation
# =============================================================================================================
# :: -u <string>    |   --user <string>:    the username of interest
# :: -s             |   --skiptime:         if given, the playback time won't be calculated
# :: -v             |   --verbose:          if given, pyFM will output WAY more information
# :: -a             |   --album:            if given, the top album of the week will be included in the output
# :: -d <int>       |   --days:             the number of days; default = 7            

def vprint(content = "", end = "\n", file = sys.stdout):
    """Only prints the content if verbose mode is active."""
    if verbose:
        print(content, end=end, file=file)


BASE_URL = "http://ws.audioscrobbler.com/2.0/" # base url of the last.fm api
SECONDS = 86400 # 1 day
DAYS = 7
TIMESPAN = SECONDS * DAYS

limit = 200 # number of songs to be analysed; max per page = 200
pages = -1 # number of pages to be fetched; default = -1
page = 1 # page pointer
includetime = True # flag for inclusion of playback time
verbose = False # Verbose mode which prints every song + execution runtine
use_spotify = False # flag for usage of Spotify's song API
output_album = False # also output the top album of the week 

current_time = time.time()

# try to import config.py file
try:
    import config
    api_key = config.api_key # LastFM api key
except ImportError:
    print("[ERROR] config.py file not found.", file=sys.stderr)
    exit(1)
except AttributeError:
    print("[ERROR] config.py doesn't contain api_key key.", file=sys.stderr)
    exit(1)

if len(config.spotify_key) > 0:
    try:
        import spotipy
        from spotipy.oauth2 import SpotifyClientCredentials
        use_spotify = True
    except ImportError as err:
        print("[ERROR] There's a missing package: " + err.msg, file=sys.stderr)
        exit(1)

argv = sys.argv[1:]
user = ""

try:
    # TODO: doc
    # get arguments, u and h, user and hashtag require value
    opts, args = getopt.getopt(argv, "u:sva", ["user=", "skiptime", "verbose", "album"])
# check if conditions are met, if not print help
except getopt.GetoptError:
    print("[ERROR] Wrong syntax. Take a look at README.md for more information.", file=sys.stderr)
    exit(1)

# handle arguments and corresponding values
for opt, arg in opts:
    if opt in ("-u", "--user"):
        user = arg
    elif opt in ("-s", "--skiptime"):
        includetime = False
    elif opt in ("-v", "--verbose"):
        verbose = True
    elif opt in ("-a", "--album"):
        output_album = True
    elif opt in ("-d", "--days"):
        DAYS = int(arg)

# Ask for username if -u (--user) wasn't given
if not user:
    user = input("User: ")

# get number of pages to be fetched
request_url = BASE_URL + "?method=user.getrecenttracks&user=" + user + "&api_key=" + api_key + "&format=json&limit=" + str(limit)
request_url += "&from=" + str(int(time.time()) - TIMESPAN)
lfm_response = requests.get(request_url).text
pages = int(json.loads(lfm_response)["recenttracks"]["@attr"]["totalPages"])

artists, albums, tracks = {}, {}, {}
mbids = {} # cache mbids and track durations
unknown_tracks = [] # save tracks without duration
totaltime = 0
track_count = 0 # because len(tracks) doesn't seem to work
known_tracks_count = 0

while page <= pages:
    # fetch latest scrobbles
    request_url = BASE_URL + "?method=user.getrecenttracks&user=" + user + "&api_key=" + api_key + "&format=json&limit=" \
        + str(limit) + "&page=" + str(page)
    request_url += "&from=" + str(int(time.time()) - TIMESPAN)
    lfm_response = requests.get(request_url).text

    # convert to python object
    lfm_json = json.loads(lfm_response)
    recent_tracks = lfm_json["recenttracks"]

    for track in recent_tracks["track"]:
        # ignore current scrobble ("nowplaying")
        if "@attr" in track:
            if "nowplaying" in track["@attr"]:
                artist = track["artist"]["#text"]
                title = track["name"]
                album = track["album"]["#text"]
                vprint("Now playling: " + artist + " - " + title + " (" + album + ")")
                continue

        date = track["date"]["#text"]
        artist = track["artist"]["#text"]
        album = track["album"]["#text"]
        title = track["name"]
        mbid = track["mbid"]
        # print(mbid)

        vprint(str(track_count + 1) + ": " + artist + " - " + title + " (" + album + ")", end="")

        # this can easily take some time as lastfm offers no bulk mode for track.getInfo
        if mbid and includetime:
            if not mbid in mbids:
                # request information for not yet cached tracks
                request_url_info = BASE_URL + "?method=track.getInfo" + "&api_key=" + api_key \
                    + "&mbid=" + mbid + "&format=json"
                lfm_response_info = requests.get(request_url_info).text
                lfm_json_info = json.loads(lfm_response_info)
                track_duration = lfm_json_info["track"]["duration"]
                mbids[mbid] = track_duration

            # use cached value instead of creating a new request
            totaltime += int(mbids[mbid])

            if int(mbids[mbid]) > 0:
                known_tracks_count += 1
            else:
                unknown_tracks.append((artist, title, album))

            if int(mbids[mbid]) > 0:
                vprint("; Duration: " + str(mbids[mbid]) + "ms")
            else:
                vprint()
        else:
            # ACHTUNG: INCLUDETIME UND MBID TRENNEN!
            unknown_tracks.append((artist, title, album))
            vprint() # add an otherwise missing line break (if verbose mode is active)

        full_title = artist + "|#|" + title
        full_album = artist + "|#|" + album

        artists[artist] = 1 if not artist in artists else artists[artist] + 1
        albums[full_album] = 1 if not full_album in albums else albums[full_album] + 1
        tracks[full_title] = 1 if not full_title in tracks else tracks[full_title] + 1

        track_count += 1

    page += 1

if use_spotify and includetime:
    # initialize Spotify API client and group/count unknown tracks
    credentials = SpotifyClientCredentials(client_id=config.spotify_key, client_secret=config.spotify_key_secret)
    client = spotipy.Spotify(client_credentials_manager=credentials)
    unknown_tracks = dict(Counter(unknown_tracks))

    # create a Spotify request
    for (artist, title, album) in list(unknown_tracks.keys()):
        query = ("track:" + title + " album:" + album + " artist:" + artist).replace("'", "")
        
        # capture output of client.search method since it might print an error message into stdout
        # (because it's ALWAYS a great idea to print error messages into stdout, forget about stderr!)
        f = io.StringIO()
        with redirect_stdout(f):
            result_set = client.search(q=query, limit=1, market=config.countrycode.upper())
        out = f.getvalue()
        if str(out).startswith("retrying"):
            vprint("[WARNING] Captured Spotify API timeout for \"%s\" - \"%s (%s)\"" % (artist, title, album))

        # request was successful; add duration(s) and delete song from dictionary of unknown songs
        if len(result_set["tracks"]["items"]) > 0:
            duration = int(result_set["tracks"]["items"][0]["duration_ms"])
            totaltime += duration * unknown_tracks[(artist, title, album)]
            vprint("Added duration (%d * %dms) for %s - %s (%s)" \
                % (unknown_tracks[(artist, title, album)], duration, artist, title, album))
            del unknown_tracks[(artist, title, album)]
        # request wasn't successful; use average as fallback
        else:
            duration = round(totaltime / track_count, 0)
            totaltime += duration
            vprint("WARNING: %s - %s (%s) not found, not even on Spotify, added %dms (current average)" \
                % (artist, title, album, duration))

    # print still unknown songs
    if verbose:
        vprint("\nStill unknown (%d songs): " % len(unknown_tracks))
        for (artist, title, album) in unknown_tracks.keys():
            vprint("%s - %s (%s)" % (artist, title, album))
        vprint()
else:
    # Approximate playback time by adding the average playback time for each unknown song
    totaltime += (track_count - known_tracks_count) * (totaltime / track_count)

# swap dictionary (str -> int -to- int -> string) and find top 3 artists
swapped_artists, swapped_tracks, swapped_albums = {}, {}, {}
ranked_artists = []

if not len(artists) > 0:
    exit(0)

for artist in artists:
    swapped_artists[artists[artist]] = artist

swapped_artists_sorted = sorted(swapped_artists.keys(), reverse=True)
while len(swapped_artists_sorted) > 3:
        swapped_artists_sorted.pop()
for i in range(len(swapped_artists_sorted)):
    ranked_artists.append(swapped_artists[swapped_artists_sorted[i]])

# exit if there were no scrobbles
if len(swapped_artists_sorted) == 0:
    exit(0)

artist_string = "artists" # plural by default
# use singular if there's only one artist
if len(swapped_artists_sorted) == 1:
    artist_string = artist_string[:-1]

# get top track and top album of the week
top_track = get_top_of(tracks)
top_album = get_top_of(albums) if output_album else None

output = f"My top {len(swapped_artists_sorted)} #lastFM {artist_string} of the last {DAYS} days:\n"

# use a fancy medal for each rank
rank = 1
for artist in ranked_artists:
    medal = None
    if rank == 1:
        medal = "🥇 "
    elif rank == 2:
        medal = "🥈 "
    else:
        medal = "🥉 "

    output += str(medal) + artist + " - " + str(artists[artist]) + " scrobbles (" \
        + str(round(artists[artist] / track_count * 100, 1)) + "%)\n"
    rank += 1

# add track of the week
# TODO: yet another option
output += "\n🎵 Top track: \"" + top_track.split("|#|")[1] + "\" by " \
    + top_track.split("|#|")[0] + " ("  + str(tracks[top_track]) + " scrobbles)" + "\n"

if output_album:
    output += "💿 Top album: \"" + top_album.split("|#|")[1] + "\" by " \
        + top_album.split("|#|")[0] + "\n"

# add playback time, if desired, else remove a then unnessecary line break
if includetime:
    output += "\n🎧 Playback time: about " + pretty_time(totaltime)
else:
    output = output[:-1] # remove unnessecary line break

# ta-daaa
print(output)
vprint("\nRuntime: " + str(round(time.time() - current_time, 2)) + " seconds")
exit()
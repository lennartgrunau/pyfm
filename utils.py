def pretty_time(milliseconds):
    """ Converts milliseconds into a human-readable time format
        separated by days, hours, minutes
        and seconds. The worst-case loss is 999ms, the best-case loss is 0ms.

        :param milliseconds: The milliseconds to convert.
        :returns: The prettified timespan.
        :rtype: string """
    seconds = int(milliseconds / 1000)
    minutes = int(seconds / 60)
    hours = int(minutes / 60)
    days = int(hours / 24)

    # get actual amount of s, m, h, d
    seconds -= minutes * 60
    minutes -= hours * 60
    hours -= days * 24

    out = "" # initialize output string

    # add seconds to string (if there are any)
    if seconds == 1:
        out = "1 second"
    elif seconds > 1:
        out = str(seconds) + " seconds"

    # add minutes
    min_str = ""
    if minutes == 1:
        min_str = "1 minute"
    elif minutes > 1:
        min_str = str(minutes) + " minutes"

    if len(out) != 0 and minutes != 0:
        min_str += ", "
    out = min_str + out

    # add hours
    hours_str = ""
    if hours == 1:
        hours_str = "1 hour"
    elif hours > 1:
        hours_str = str(hours) + " hours"

    if len(out) != 0 and hours != 0:
        hours_str += ", "
    out = hours_str + out

    # add days
    days_str = ""
    if days == 1:
        days_str = "1 day"
    elif days > 1:
        days_str = str(days) + " days"

    if len(out) != 0 and days != 0:
        days_str += ", "
    out = days_str + out

    # find the last occurence of a comma and replace it with a fancy "and"
    out = out[:out.rfind(", ")] + " and " + out[out.rfind(", ") + 2:]
    return out


def get_top_of(dictionary):
    """ Swaps keys and values of a dictionary, sorts the former
        values and returns the first one.
        :param dictionary: The dictionary that the function
        shall be applied on.
        :returns: The "top" item of the (new) dictionary. """
    swapped_dictionary = dict()
    for item in dictionary:
        swapped_dictionary[dictionary[item]] = item
    
    swapped_dictionary[dictionary[item]] = item
    swapped_dictionary_keys = list(sorted(swapped_dictionary.keys(), reverse=True))
    top = swapped_dictionary[swapped_dictionary_keys[0]]
    return top
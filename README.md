# pyFM

A Python script that outputs your top 3 [last.fm](https://last.fm) artists of the week and your total playback time.

## Requirements

* Python 3.6+
* last.fm account (obviously)
* last.fm API key (can be requested [here](https://last.fm/api/account/create))
* (Optional) Spotify API key (can be requested [here](https://developer.spotify.com))

## Setup

* Install all required modules: `pip install -r requirements.txt`  
* Rename `config.py.template` to `config.py` and put your API key(s) (and your country code) in it

### About the config.py file

The config.py file contains 4 variables: `api_key`, `spotify_key`, `spotify_key_secret` and `countrycode`.

* `api_key` is your last.fm API key. You definitely need this one.
* `spotify_key` and `spotify_secret` hold your Spotify API key and its secret token. You don't need these two keys if you don't want to use the Spotify API (read more about its purpose at the end of this file).
* `countrycode` is your [ISO-3166-1/ALPHA-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code. This one is completely optional, however if you're using the Spotify API, it is recommended to provide it since otherwise Spotify might not recognize some of your played songs.

## Usage

### Syntax description

`python pyfm.py [[-u | --user] <string>] [[-h | --hashtag] <string>] [-s | --skip] [-v | --verbose]`

All these values are optional in theory, but if you don't provide a user name, you will be immediately prompted to enter one. However, there is really no reason not to provide it directly.

### Argument explanations

**Shorttag** | **Longtag** | **Value type** | **Explanation** | **Example**
--- | --- | --- | --- | ---
-u | --user | string | The user name of interest. | --user johndoe
-h | --hashtag | string | A custom hashtag (without "#"). | -h johnFM
-s | --skiptime | (nothing) | If given, the playback time won't be calculated. | -s
-v | --verbose | (nothing) | If given, pyFM will print **__WAY__** more information about what it does. | --verbose

Hint: don't use `-v` or `--verbose` if you want to combine pyFM with some Twitter API service like [pyTweet](https://gitlab.com/slndrmn/pytweet) or any other service that has a Twitter-like character limitation.

### Some examples

* Get the statistics for johndoe:  
`python pyfm.py -u johndoe`
* Get the statistics for johndoe and use johnFM as custom hashtag:  
`python pyfm.py -u johndoe --hashtag johnFM`
* Get just the top 3 artists of johndoe and use johnFM as custom hashtag:  
`python pyfm.py --user johndoe -h johnFM --skiptime`

## Output

The output should look like this:

```
#lastFM - Top 3 artists of the week:
🥇 The John Doe Band - 100 scrobbles (16.3%)
🥈 Mewsick - 46 scrobbles (7.5%)
🥉 Rick Astley - 31 scrobbles (5.1%)

🎧 Playback time: about 3 hours, 5 minutes and 57 seconds
```

### About the total playback time and the role of Spotify

The total playback time is received by creating an API request for *each* song that you've listened to within the last seven days, asking for information about these song, extracting the duration and summing it up. Since last.fm doesn't provide a way to get the duration of a bunch of songs simultaneously, not just a single one, each song leads to a new API request, which unfortunately takes some time. However, as explained above, you can skip this step by adding `-s` or `--skiptime` as a shell argument.  
Also, last.fm provides song durations in milliseconds, which is fine but not pretty at all. In order to fix this, *pyFM* transforms the total playback time into a human-readable, prettified version which separated days, hours, minutes and seconds. The worst-case loss of the current method is 999ms, the best-case loss is 0ms.

Another problem is that last.fm is missing a lot of songs or doesn't know their duration. If this occurs, one of these three things will happen:

1. You don't have provided any Spotify API keys.  
In this case, pyFM takes the average duration of a song and adds it to the total playback time.
2. You have provided your Spotify API keys, but Spotify doesn't recognize the song either  
In this case, pyFM also takes the average duration of a song and adds it to the total playback time.
3. You have provided your Spotify API keys and Spotify recognizes the song  
In this case, we've got the duration of the song and can just add it to the total playback time.

## Special thanks

* [@itsmelenni](https://twitter.com/itsmelenni) - For participating on this project
* _to be (hopefully) continued soon_
